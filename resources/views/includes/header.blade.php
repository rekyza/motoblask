<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="home"></a>
    <button class="navbar-toggler ml-auto custom-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div id="navbarNavDropdown" class="navbar-collapse collapse">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="news">Aktualności</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/#fast-contact">Cennik</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact">Wulkanizacja</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://allegro.pl/listing/user/listing.php?us_id=38386040&order=m">Filtry i oleje</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact">Kontakt</a>
            </li>
        </ul>
    </div>
</nav>

<div id="image-slider-1" class="col-lg-6">
    <div id="video-carousel-example" class="carousel slide carousel-fade" data-ride="carousel" data-pause="false">
        <!--Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#video-carousel-example" data-slide-to="0" class="active"></li>
            <li data-target="#video-carousel-example" data-slide-to="1"></li>
            <li data-target="#video-carousel-example" data-slide-to="2"></li>
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" data-pause="false" role="listbox">
            <div class="carousel-item active">
                <div class="header-image">
                    before
                </div>
                <img src="{{URL::asset('/img/before.jpg')}}" class="img-fluid" alt="profile Pic">
            </div>
            <div class="carousel-item">
                <div class="header-image">
                    after
                </div>
                <img src="{{URL::asset('/img/after.jpg')}}" class="img-fluid" alt="profile Pic">
            </div>
            <div class="carousel-item">
                <div class="header-image">
                    before
                </div>
                <img src="{{URL::asset('/img/before.jpg')}}" class="img-fluid" alt="profile Pic">
            </div>
        </div>
        <!--/.Slides-->
    </div>
</div>
<div id="image-slider-2" class="col-lg-6">
    <div id="video-carousel-example" class="carousel slide carousel-fade" data-ride="carousel" data-pause="false">
        <!--Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#video-carousel-example" data-slide-to="0" class="active"></li>
            <li data-target="#video-carousel-example" data-slide-to="1"></li>
            <li data-target="#video-carousel-example" data-slide-to="2"></li>
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" data-pause="false" role="listbox">
            <div class="carousel-item active">
                <div class="header-image">
                    after
                </div>
                <img src="{{URL::asset('/img/after.jpg')}}" class="img-fluid" alt="profile Pic">
            </div>
            <div class="carousel-item">
                <div class="header-image">
                    before
                </div>
                <img src="{{URL::asset('/img/before.jpg')}}" class="img-fluid" alt="profile Pic">
            </div>
            <div class="carousel-item">
                <div class="header-image">
                    after
                </div>
                <img src="{{URL::asset('/img/after.jpg')}}" class="img-fluid" alt="profile Pic">
            </div>
        </div>
        <!--/.Slides-->
    </div>
</div>
