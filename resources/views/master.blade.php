<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="container-fluid">

    <header class="row">

        @include('includes.header')

    </header>

    <main class="row justify-content-center">

        @include('pages.welcome')

    </main>

    <div class="row justify-content-center">

        @include('pages.pricelist')

    </div>

    <div class="row justify-content-center">

        @include('pages.contact')

    </div>

    <footer class="row">

        @include('includes.footer')

    </footer>

</div>
</body>
</html>
