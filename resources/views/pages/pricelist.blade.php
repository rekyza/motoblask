<div id="fast-contact" class="row justify-content-center">
        <div class="col-6">
            <img src="{{URL::asset('/img/contact.png')}}" class="img-container" class="col-6" id="contact-img" alt="contact">
        </div>
        <div class="col-6">
            <div id="text-fast-contact">
                <h4 class="align-middle" class="col-12">Zobaczyłeś zdjęcia umytych aut i nie chcesz dłużej zwlekać?</h4>
                <h2 class="align-middle" class="col-12">Zadzwoń i umów się na wizytę!</h2>
                <h1 class="align-middle" class="col-12">661 553 600</h1>
            </div>
        </div>
</div>
<div id="pricelist" class="container-fluid">
        <div id = "pot" class="container-fluid">
            <img src="{{URL::asset('/img/bubbles.png')}}" class="img-container" id="contact-img" alt="bubbles water">
        </div>

        <table class="table table-striped table-dark">
            <thead>
            <tr>
                <th scope="col">mycie</th>
                <th scope="col">full</th>
                <th scope="col">pranie</th>
                <th scope="col"></th>
                <th scope="col">wosk</th>
                <th scope="col">glinka</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>20 zł</td>
                <td>40 zł</td>
                <td>150 zł</td>
                <td><a href="#pricelist-text"><img src="{{URL::asset('/img/1.png')}}" class="img-fluid" alt="mini car"></a></td>
                <td>150 zł</td>
                <td>50 zł</td>
            </tr>
            <tr>
                <td>25 zł</td>
                <td>45 zł</td>
                <td>180 zł</td>
                <td><a href="#pricelist-text"><img src="{{URL::asset('/img/2.png')}}" class="img-fluid" alt="small car"></a></td>
                <td>180 zł</td>
                <td>60 zł</td>
            </tr>
            <tr>
                <td>30 zł</td>
                <td>50 zł</td>
                <td>220 zł</td>
                <td><a href="#pricelist-text"><img src="{{URL::asset('/img/3.png')}}" class="img-fluid" alt="car"></a></td>
                <td>220 zł</td>
                <td>80 zł</td>
            </tr>
            <tr>
                <td>35 zł</td>
                <td>60 zł</td>
                <td>250 zł</td>
                <td><a href="#pricelist-text"><img src="{{URL::asset('/img/4.png')}}" class="img-fluid" alt="big car"></a></td>
                <td>220 - 330 zł</td>
                <td>100 zł</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div id="pricelist-text" class="row justify-content-center">
        <div id="services_text" class="text-center">
            Jeśli chcesz kompleksowo umyć auto z zewnątrz to interesuje Cię nasza usługa <b>mycie</b></br><hr>
            Wybierz <b>fulla</b> umyjemy auto z zewnątrz i wewnątrz, poodkurzamy, wysuszymy i nałożymy czernidło.</br><hr>
            <b>Pranie</b> to fachowe wypranie i wysuszenie Twojej tapicerki.</br><hr>
            <b>Wosk</b> to szeryfa oczko w głowie, sprawdź naszą usługę woskowania twardego, a nie poznasz własnego auta.</br><hr>
            <b>Glinkowanie</b> to zabieg, który można porównać do peelingu skóry. Dzięki niemu powłoka lakiernicza jest gładka i lepiej chroniona przed brudem.
        </div>
    </div>
